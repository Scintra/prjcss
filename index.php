<!DOCTYPE html>
<html>

<head>
   <title>Cadastro de Visitas</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width inicial-scale=1.0">
   <link href="style.css" rel="stylesheet" type="text/css" />
<script>
   function mostrarResultado(str){
      if(str.length == 0) {
      document.getElementById("divbuscar").innerHTML = "";
      document.getElementById("divbuscar").style.border = "0px";
      return;
      }
      if(window.XMLHttpRequest) {

      xmlhttp = new XMLHttpRequest();
      //ie7+, firefox. chrome , opera e safari
      } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      //so para ie6 e ie5
      }

      xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
       document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
      document.getElementById("divbuscar").style.border = "1px solid #a5acb2";

      }
   }

   xmlhttp.open("GET", "buscar.php?q=" + str, true);
   xmlhttp.send();
}
</script>
</head>

<body>
<?php include("menu.php") ?>
   <h1>Livro de Visitas</h1>
   <fieldset>
      <legend> Efetue o cadastro:</legend>
      <form id="form1" name="form1" method="post" action="valida.php">
         <p>Nome:<br />
            <input name="nome" required placeholder="Entre com seu nome" id="nome" type="text" />
         </p>
         <p>Endereço:<br />
            <input name="endereco" required placeholder="Entre com o endereço" id="endereco" type="text" />
         </p>
         <p>Telefone:<br />
            <input name="telefone" required placeholder="Entre com o telefone" id="telefone" type="text" />
         </p>
         <p>E-mail:<br />
            <input name="email" required id="email" placeholder="Entre com o e-mail" type="email" />
         </p>
         <p>
            <input name="enviar" id="enviar" value="Enviar" type="submit" />
         </p>


      </form>
   </fieldset>

   <h2> Escreva sua consulta ... </h2>
 <form name = "frm_consulta" id = "frm_consulta">
 <input onkeyup= "mostrarResultado(this.value)" name = "busca" id = "busca" type="text" placeholder="Digite um nome..."/>
 </form>

<h3>Clientes Cadastrados</h3>

<?php
include("conecta.php");
$sql = "SELECT nome_visita, endereco_visita, telefone_visita, email_visita, cod_visita FROM tb_visitas";
$res = mysqli_query($_con, $sql);

print "<div id='divbuscar'>";

print "
<table width='100%' border='0'>
<tr>
<th class='tabela'>Nome</th>
<th class='tabela'>Endereço</th>
<th class='tabela'>Telefone</th>
<th class='tabela'>E-mail</th>
<th class='tabela'>Editar</th>
<th class='tabela'>Apagar</th>
</tr>
";
while ($linha = mysqli_fetch_array($res)){
   print "
      <tr class='marca_linha'>
         <td class='tabela'>$linha[0]</td>
         <td class='tabela'>$linha[1]</td>
         <td class='tabela'>$linha[2]</td>
         <td class='tabela'>$linha[3]</td>
         <td class='tabela'><a href='editar.php?cod=$linha[4]'>Editar</a></td>
         <td class='tabela'><a href='apagar.php?cod=$linha[4]'>Apagar</a></td>
      </tr>

   ";
}

print "</table></div>";

mysqli_close($_con);

?>

</body>

</html>