

<!DOCTYPE html>
<html>

<head>
   <title>Cadastro de Visitas</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width inicial-scale=1.0">
   <link href="style.css" rel="stylesheet" type="text/css" />

   <script>
   function mostrarResultado(str){
      if(str.length == 0) {
      document.getElementById("divbuscar").innerHTML = "";
      document.getElementById("divbuscar").style.border = "0px";
      return;
      }
      if(window.XMLHttpRequest) {

      xmlhttp = new XMLHttpRequest();
      //ie7+, firefox. chrome , opera e safari
      } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      //so para ie6 e ie5
      }

      xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
       document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
      document.getElementById("divbuscar").style.border = "1px solid #a5acb2";

      }
   }

   xmlhttp.open("GET", "buscarcidade.php?q=" + str, true);
   xmlhttp.send();
}
</script>

</head>

<body>
<?php include("menu2.php"); ?> 
    
   <h1>Cadastro de Cidades</h1>
   <fieldset>
      <legend> Efetue o cadastro da cidade:</legend>
      <form id="form1" name="form1" method="post" action="valida_cidade.php">
         <p>Nome:<br />
            <input name="nome_cidade" required placeholder="Entre com o nome da cidade" id="nome_cidade" type="text" />
         </p>
         <p>
            <input name="enviar" id="enviar" value="Enviar" type="submit" />
         </p>
      </form>
   </fieldset>

   <h2> Consulte sua cidade... </h2>
<form name = "frm_consulta" id = "frm_consulta">
<input onkeyup= "mostrarResultado(this.value)" name = "busca" id = "busca" type="text" placeholder="Digite um nome..."/>
</form>

   <h3>Cidades Cadastradas</h3>

<?php
include("conecta.php");



$sql = "SELECT nome_cidade, cod_cidade FROM tb_cadastrocidade";
$res = mysqli_query($_con, $sql);


print "<div id='divbuscar'>"; 

print "
<table width='100%' border='0'>
<tr>
<th class='tabela'>Nome Cidade</th>
<th class='tabela'>Editar</th>
<th class='tabela'>Apagar</th>
</tr>
";
while ($linha = mysqli_fetch_array($res)){
   print "
      <tr class='marca_linha'>
         <td class='tabela'>$linha[0]</td>
         <td class='tabela'><a href='editar_cidade.php?cod=$linha[1]'>Editar</a></td>
         <td class='tabela'><a href='apagar_cidade.php?cod=$linha[1]'>Apagar</a></td>
      </tr>
   ";
}

print "</table></div>";

mysqli_close($_con);

?>


</body>

</html>